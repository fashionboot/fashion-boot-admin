export const environment = {
  production: false,
  baseUrl: 'https://fashion-boot-prod.herokuapp.com',
  // baseUrl: 'http://localhost:8081',
  keycloak: {
    // Url of the Identity Provider
    issuer: 'https://key.mxti.com.br/auth',
    // Realm
    realm: 'fashionboot',
    clientId: 'fashonbootadmin',
  },
};
