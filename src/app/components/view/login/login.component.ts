import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Usuario} from "../../../interfaces/interfaces";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // @ts-ignore
  usuario: Usuario = {
    id:"",
    email:"",
    password:"",
};

  constructor(private fb: FormBuilder) {
  }

  mail = new FormControl("" ,[Validators.required])
  password = new FormControl("" ,[Validators.required])

  ngOnInit(): void {

  }

}
