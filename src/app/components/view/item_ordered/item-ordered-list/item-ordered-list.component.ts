import { Component, OnInit } from '@angular/core';
import {ItemOrderedService} from "../../../../services/item-ordered.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ItemOrdered} from "../../../../interfaces/interfaces";

@Component({
  selector: 'app-item-ordered-list',
  templateUrl: './item-ordered-list.component.html',
  styleUrls: ['./item-ordered-list.component.css']
})
export class ItemOrderedListComponent implements OnInit {

  displayedColumns: string[] = ["id", "quantidade", "produto", "produtoid"];

  id_cat: String = "";
  itemOrdem: ItemOrdered[] = [];

  constructor(
    private service: ItemOrderedService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.id_cat = this.route.snapshot.paramMap.get("id")!;
    this.findAll();
  }

  findAll(): void {
    this.service.findAllItemOrderedByOrdemId(this.id_cat).subscribe((resposta) => {
      this.itemOrdem = resposta;
    });
  }

  voltar(): void {
    this.router.navigate([`/pedidos`])
  }
}
