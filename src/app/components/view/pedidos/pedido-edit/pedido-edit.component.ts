import {Component, OnInit} from '@angular/core';
import {Address, Ordem, Usuario} from "../../../../interfaces/interfaces";
import {OrdemService} from "../../../../services/ordem.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-pedido-edit',
  templateUrl: './pedido-edit.component.html',
  styleUrls: ['./pedido-edit.component.css']
})
export class PedidoEditComponent implements OnInit {


  constructor(private ordemService: OrdemService, private router: Router,   private route: ActivatedRoute,) {
  }

  buttonDisable: boolean = false;
  //@ts-ignore
  usuario: Usuario = {
    id: '',
    name: '',
    email: '',
    phone: '',

  };

  //@ts-ignore
  pedido: Ordem = {
    id: "",
    totalPrice: "",
    usuario: this.usuario,
    date_purchase: "",
    status: "",

  }

  status = new FormControl("", [Validators.minLength(1)]);
  id = new FormControl("", [Validators.minLength(1)]).disabled;
  email = new FormControl("", [Validators.minLength(1)]).disabled;

  ngOnInit(): void {
    this.pedido.id = this.route.snapshot.paramMap.get("id")!;
    this.ordemService.findById(this.pedido.id!).subscribe(resposta => {

      this.pedido = resposta;
      console.log( this.pedido);
    }, error => {

    })
  }

  getMessage() {
      if (this.status.invalid) {
      return "O campo deve conter pelo menos 1 letra";
    }

    return false;
  }

  create() {
 this.buttonDisable = true
    this.ordemService.update(this.pedido).subscribe(resposta => {
      this.buttonDisable = false
      this.router.navigate(['pedidos'])
    }, error => {
      this.buttonDisable = false
    })

  }

  cancel() {
    this.router.navigate(['pedidos'])

  }
}
