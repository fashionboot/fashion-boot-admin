import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Ordem} from "../../../../interfaces/interfaces";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Router} from "@angular/router";
import {fromEvent, merge} from "rxjs";
import {debounceTime, distinctUntilChanged, tap} from "rxjs/operators";
import {OrdemService} from "../../../../services/ordem.service";
import {DataBaseOrdem} from "../../../../services/data-base-ordem";

@Component({
  selector: 'app-pedido-list',
  templateUrl: './pedido-list.component.html',
  styleUrls: ['./pedido-list.component.css']
})
export class PedidoListComponent implements OnInit, AfterViewInit {

  displayedColumns: any[] = ["codigo", "cliente", "status", "data de compra","Ordem Info","valor", "acoes"];
  ordem: Ordem[] = [];
  // @ts-ignore
  dataSource: DataBaseOrdem;
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild('input') input: ElementRef;

  constructor(private service: OrdemService, private router: Router) {
  }

  ngOnInit(): void {
    this.dataSource = new DataBaseOrdem(this.service);
    this.dataSource.loadPedidos('', 'asc', 0, 5);
    this.findAll();
  }

  findAll() {
    this.service.findAll().subscribe((resposta) => {
      this.ordem = resposta;
    });
  }

  ngAfterViewInit(): void {
    // server-side search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),

        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadProdutosPage();
        })
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadProdutosPage())
      )
      .subscribe();
  }


  loadProdutosPage() {

    this.dataSource.loadPedidos(
      this.input.nativeElement.value,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  NavegarParaOrdemInfo() {

  }
}
