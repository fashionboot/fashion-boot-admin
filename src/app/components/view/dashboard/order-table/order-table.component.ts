import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { OrderByDayDTO } from 'src/app/interfaces/interfaces';
import { OrdemService } from 'src/app/services/ordem.service';

@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.css']
})
export class OrderTableComponent implements OnDestroy, OnInit {

  dtOptions: DataTables.Settings = {};
  ordersByStatusDTO: OrderByDayDTO[] = [];

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject<any>();

  constructor(
    private httpClient: HttpClient,
    private service: OrdemService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true


    };
    this.loadTable();
  }

  loadTable() {
    this.service.findOrderByDay().subscribe((response) => {
      this.ordersByStatusDTO = response;
      this.dtTrigger.next();
    })

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}
