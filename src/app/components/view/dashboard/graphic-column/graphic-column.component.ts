import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import * as Highcharts from 'highcharts';
import { SaleByMonthDTO } from 'src/app/interfaces/interfaces';
import { OrdemService } from 'src/app/services/ordem.service';

@Component({
  selector: 'app-graphic-column',
  templateUrl: './graphic-column.component.html',
  styleUrls: ['./graphic-column.component.css']
})
export class GraphicColumnComponent implements OnInit {

  public salesByMonth: SaleByMonthDTO[] = [];

  public column = new  Chart({
    title: {
      text: 'Vendas por mês'
    },
    credits: {
      enabled: false
    },
    yAxis: {
      title: {
        text: 'Vendas'
      }
    },
    tooltip: {
      formatter: function () {
          return 'Foram vendidos <b>R$ ' + Highcharts.numberFormat(this.y, 2, ',', '.') + '</b> no mês de <b>' + this.series.name + '</b>';
      }
    },
    series: []
    });

  constructor(private service: OrdemService) {

  }

  ngOnInit(): void {
    this.loadGraphic();
  }

  loadGraphic() {
    this.service.findSaleByMonth().subscribe((response) => {
      this.salesByMonth = response;
      this.loadSeriesOnGraphic();
    });
  }

  loadSeriesOnGraphic(){

    this.salesByMonth.map(x => {
      this.column.addSeries({
        type: 'column',
        name: x.monthDescription,
        data: [x.totalValue]
      },true, true)
    });

  }

}
