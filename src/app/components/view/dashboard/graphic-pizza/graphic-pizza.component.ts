import { OrderByStatusDTO } from './../../../../interfaces/interfaces';
import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { OrdemService } from 'src/app/services/ordem.service';
import { PointOptionsObject } from 'highcharts';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-graphic-pizza',
  templateUrl: './graphic-pizza.component.html',
  styleUrls: ['./graphic-pizza.component.css']
})
export class GraphicPizzaComponent implements OnInit {

  public ordersByStatusDTO: OrderByStatusDTO[] = [];

  public pieData: Array<(number | [string, (number | null)] | null | PointOptionsObject)> = [];

  public pie = new Chart({
    title: {
      text: 'Pedidos X status no mês atual'
    },
    credits: {
      enabled: false
    },
    tooltip: {
      formatter: function () {
          return 'O total de pedidos com status <b>' + this.key +
              '</b> é <b> R$ ' + Highcharts.numberFormat(this.y, 2, ',', '.') + ' </b>';
      }
    },
    series: []
    });

  constructor(private service: OrdemService) {

  }

  ngOnInit(): void {
    this.loadGraphic();
  }

  loadGraphic() {
    this.service.findOrderByStatus().subscribe((response) => {
      this.ordersByStatusDTO = response;
      this.pie.addSeries({
        name: 'Pedidos X status do mês atual',
        type: 'pie',
        data: this.convertToGraphic()
      }, true, true);
    });
  }


  convertToGraphic(){

    this.ordersByStatusDTO.map(x => {
      console.log(x.quantity);
      this.pieData.push({
        color: x.color,
        name: x.status,
        y: x.quantity
      })

    });

    return this.pieData;

  }
}
