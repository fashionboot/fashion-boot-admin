import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {Brand, Category, Gallery, Options, Product} from "../../../../interfaces/interfaces";
import {
  HttpClient,
  HttpErrorResponse,
  HttpEvent,
  HttpEventType,
  HttpHeaders, HttpParams,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import {catchError, finalize, map} from "rxjs/operators";
import {Observable, of, Subscription} from "rxjs";
import {ProdutoService} from "../../../../services/produto.service";
import {MarcaService} from "../../../../services/marca.service";
import {CategoriaService} from "../../../../services/categoria.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-produto-create',
  templateUrl: './produto-create.component.html',
  styleUrls: ['./produto-create.component.css']
})
export class ProdutoCreateComponent implements OnInit {


  buttonDisable: boolean = false;

  // @ts-ignore
  @ViewChild('checkP') tamanhoP;
  // @ts-ignore
  @ViewChild('checkM') tamanhoM;
  // @ts-ignore
  @ViewChild('checkG') tamanhoG;
  // @ts-ignore
  @ViewChild('checkGG') tamanhoGG;

  spiner: boolean = true;

  constructor(private http: HttpClient,
              private uploadService: ProdutoService,
              private marcaService: MarcaService,
              private cateService: CategoriaService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.marcaService.findAllBrands().subscribe((resposta) => {
      this.brandd = resposta;
    })
    this.cateService.findAllCategorias().subscribe((resposta) => {
      this.cate = resposta;
    })
  }


  // @ts-ignore
  @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;
  files = [];

  // @ts-ignore


  uploadFile(file) {
    this.buttonDisable = true;
    this.spiner = false;
    if (this.tamanhoP.checked == true) {
      this.product.options?.push(
        {name: "P", value: "P"}
      )
    }
    if (this.tamanhoM.checked == true) {
      this.product.options?.push(
        {name: "M", value: "M"}
      )
    }
    if (this.tamanhoG.checked == true) {
      this.product.options?.push(
        {name: "G", value: "G"}
      )
    }
    if (this.tamanhoGG.checked == true) {
      this.product.options?.push(
        {name: "GG", value: "GG"}
      )
    }

    const formData = new FormData();

    this.files.forEach(file => {
      // @ts-ignore
      formData.append('file', new Blob([file.data], {type: 'application/octet-stream'}), file.data.name);
    });

    formData.append('produto', new Blob([JSON.stringify(this.product)], {type: 'application/json'}));
    this.uploadService.upload(formData)
      .subscribe(resposta => {

        //tratando se o button fica desabilitado
        this.buttonDisable = false;
        //tratando se o spinner fica desabilitado
        this.spiner = true;
      this.router.navigate([`produtos`]);
      this.uploadService.mensagem("Produto Criado com Sucesso");
        this.spiner = true;
    }, err => {
        //tratando se o button fica desabilitado
        this.buttonDisable = false;
        //tratando se o spinner fica desabilitado
        this.spiner = true;
      this.uploadService.mensagem(err.error.error);
    });
  }

  uploadFiles() {
    this.fileUpload.nativeElement.value = '';
    // this.files.forEach(file => {
    this.uploadFile(this.files[0]);
    // });
  }

  onClick() {
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        // @ts-ignore
        this.files.push({data: file, inProgress: false, progress: 0});
      }

    };
    fileUpload.click();
  }


  brandd: Brand[] = [];
  cate: Category[] = [];
  category: Category = {

    name: "",
    redirect: ""

  }
  brand: Brand = {

    name: "",
  }
  gallery: Gallery[] = []
  options: Options[] = [];

  product: Product = {
    name: "",
    description: "",
    detail: "",
    category: this.category,
    brand: this.brand,
    img: "",
    gallery: this.gallery,
    onSale: true,
    salePrice: "",
    options: this.options,
    inStock: true,
    gender: "",
  }
  name = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(50)])
  description = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(100)])
  detail = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(500)])
  categoria = new FormControl("")//, [Validators.required, Validators.minLength(3), Validators.maxLength(30)])
  marca = new FormControl("")//, [Validators.required, Validators.minLength(3), Validators.maxLength(30)])
  img = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(300)])
  onSale = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(5)])
  salePrice = new FormControl("", [Validators.required, Validators.maxLength(30)])
  inStock = new FormControl("", [Validators.required, Validators.minLength(3)])
  gender = new FormControl("", [Validators.required])


  getMessage() {
    if (this.name.invalid) {
      return "O campo NOME deve conter entre 3 e 50 carateres";
    }

    if (this.description.invalid) {
      return "O campo DESCRIÇÃO deve conter entre 3 e 100 carateres";
    }

    if (this.detail.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.categoria.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.marca.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.onSale.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.salePrice.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.inStock.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.gender.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }

    return false;
  }

  cancel() {
    this.router.navigate(['produtos'])
  }

}

