import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Brand, Category, Gallery, Options, Product} from "../../../../interfaces/interfaces";
import {ProdutoDataSource} from "../../../../services/data-base-produto";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {ProdutoService} from "../../../../services/produto.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl, Validators} from "@angular/forms";
import {MarcaService} from "../../../../services/marca.service";
import {CategoriaService} from "../../../../services/categoria.service";

@Component({
  selector: 'app-produto-update',
  templateUrl: './produto-update.component.html',
  styleUrls: ['./produto-update.component.css']
})
export class ProdutoUpdateComponent implements OnInit {

  id_cat: String = "";
  buttonDisable: boolean = false;
  ccheckP: boolean = false;
  ccheckM: boolean = false;
  ccheckG: boolean = false;
  ccheckGG: boolean = false;

  // @ts-ignore
  @ViewChild('checkP') tamanhoP;
  // @ts-ignore
  @ViewChild('checkM') tamanhoM;
  // @ts-ignore
  @ViewChild('checkG') tamanhoG;
  // @ts-ignore
  @ViewChild('checkGG') tamanhoGG;

  spiner: boolean = true;

  // @ts-ignore
  dataSource: ProdutoDataSource;
  displayedColumns: string[] = ["id", "name", "detail", "category", "brand", "onSale", "salePrice", "inStock", "gender", "acoes"];
  fotosColumns: string[] = ["id", "link", "acoes"];
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild('input') input: ElementRef;

  constructor(private service: ProdutoService, private route: ActivatedRoute, private router: Router, private marcaService: MarcaService,
              private cateService: CategoriaService,) {

  }

  ngOnInit(): void {
    this.id_cat = this.route.snapshot.paramMap.get("id")!;
    this.findById()
    this.marcaService.findAllBrands().subscribe((resposta) => {
      this.brandd = resposta;
    })
    this.cateService.findAllCategorias().subscribe((resposta) => {
      this.cate = resposta;
    })
    this.service.findGalleryImagensByProdutoId(this.id_cat).subscribe(resposta => {
      this.gallery = resposta;

    })

  }

  // @ts-ignore
  @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;
  files = [];

  // @ts-ignore
  uploadFile(file) {
    this.service.deleteOptions(this.id_cat).subscribe();
    this.buttonDisable = true;
    this.spiner = false;
    if (this.tamanhoP.checked == true) {
      this.product.options?.push(
        {name: "P", value: "P"}
      )
    }
    if (this.tamanhoM.checked == true) {
      this.product.options?.push(
        {name: "M", value: "M"}
      )
    }
    if (this.tamanhoG.checked == true) {
      this.product.options?.push(
        {name: "G", value: "G"}
      )
    }
    if (this.tamanhoGG.checked == true) {
      this.product.options?.push(
        {name: "GG", value: "GG"}
      )
    }

    const formData = new FormData();

    this.files.forEach(file => {
      // @ts-ignore
      formData.append('file', new Blob([file.data], {type: 'application/octet-stream'}), file.data.name);
    });

    formData.append('produto', new Blob([JSON.stringify(this.product)], {type: 'application/json'}));
    this.service.uploadReplace(formData)
      .subscribe(resposta => {
        this.router.navigate([`produtos`]);
        this.service.mensagem("Produto editado com Sucesso");
        //tratando se o button fica desabilitado
        this.buttonDisable = false;
        //tratando se o spinner fica desabilitado
        this.spiner = true;
      }, err => {
        for(let i = 0; i < err.error.errors.length; i++) {
        this.service.mensagem(err.error.errors[i].message)
          //tratando se o button fica desabilitado
          this.buttonDisable = false;
          //tratando se o spinner fica desabilitado
          this.spiner = true;
        }
      });
  }

  uploadFiles() {
    this.fileUpload.nativeElement.value = '';
    // this.files.forEach(file => {
      this.uploadFile(this.files[0]);
    // });
  }

  onClick() {
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        // @ts-ignore
        this.files.push({data: file, inProgress: false, progress: 0});
      }

    };
    fileUpload.click();
  }


  findById(): void {
    this.service.findById(this.id_cat!).subscribe((resposta) => {
      resposta.options?.forEach(value => {
        if (value.name == 'P') {
          this.ccheckP = true;
        }
        if (value.name == 'M') {
          this.ccheckM = true;
        }
        if (value.name == 'G') {
          this.ccheckG = true;
        }
        if (value.name == 'GG') {
          this.ccheckGG = true;
        }
      })
      this.product = resposta;


    });
  }


  brandd: Brand[] = [];
  cate: Category[] = [];
  category: Category = {

    name: "",
    redirect: ""

  }
  brand: Brand = {

    name: "",
  }
  gallery: Gallery[] = []
  options: Options[] = [];

  product: Product = {
    name: "",
    description: "",
    detail: "",
    category: this.category,
    brand: this.brand,
    img: "",
    gallery: this.gallery,
    onSale: true,
    salePrice: "",
    options: this.options,
    inStock: true,
    gender: "",
  }
  name = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(50)])
  description = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(100)])
  detail = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(500)])
  categoria = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(30)])
  marca = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(30)])
  img = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(300)])
  onSale = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(5)])
  salePrice = new FormControl("", [Validators.required, Validators.maxLength(30)])
  inStock = new FormControl("", [Validators.required, Validators.minLength(3)])
  gender = new FormControl("", [Validators.required])


  getMessage() {
    if (this.name.invalid) {
      return "O campo NOME deve conter entre 3 e 50 carateres";
    }

    if (this.description.invalid) {
      return "O campo DESCRIÇÃO deve conter entre 3 e 100 carateres";
    }

    if (this.detail.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.categoria.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.marca.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.onSale.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.salePrice.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.inStock.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }
    if (this.gender.invalid) {
      return "O campo TEXTO deve conter entre 10 e 500 carateres";
    }

    return false;
  }


  // @ts-ignore
  deleteFoto(id) {
    this.service.deleteGalleryById(id).subscribe(resposta => {
      this.service.findGalleryImagensByProdutoId(this.id_cat).subscribe(res => {
        this.gallery = res;
        for (let i = 0; i < this.gallery.length; ++i) {
          if (this.gallery[i].id === id) {
            this.gallery.splice(i, 1);
          }
        }
      })

    })

  }

  cancel() {
    this.router.navigate(['produtos'])
  }
}

