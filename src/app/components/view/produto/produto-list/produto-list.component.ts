import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { ActivatedRoute, Router } from "@angular/router";
import { fromEvent, merge } from "rxjs";
import { debounceTime, distinctUntilChanged, tap } from "rxjs/operators";
import { AuthService } from "../../../../guard/auth.service";
import { Product } from "../../../../interfaces/interfaces";
import { ProdutoDataSource } from "../../../../services/data-base-produto";
import { ProdutoService } from "../../../../services/produto.service";


@Component({
  selector: 'app-produto-list',
  templateUrl: './produto-list.component.html',
  styleUrls: ['./produto-list.component.css']
})
export class ProdutoListComponent implements OnInit, AfterViewInit {

  id_cat: String = "";
  produtos: Product[] = [];
  // @ts-ignore



  // @ts-ignore
  dataSource: ProdutoDataSource;
  displayedColumns: string[] = ["id", "name", "category", "brand", "onSale", "salePrice", "inStock", "gender", "acoes"];
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild('input') input: ElementRef;

  constructor(private service: ProdutoService, private route: ActivatedRoute,  private router: Router, private authService: AuthService) {

  }

  ngOnInit(): void {
    this.id_cat = this.route.snapshot.paramMap.get("id")!;
    this.dataSource = new ProdutoDataSource(this.service);
    this.dataSource.loadProdutos('', 'asc', 0, 10);
    this.findAll();
    //console.log(this.authService.getLoggedUser())
    //console.log(this.authService.getToken())
  }

  ngAfterViewInit() {

    // server-side search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),

        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadProdutosPage();
        })
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadProdutosPage())
      )
      .subscribe();
  }


  loadProdutosPage() {

    this.dataSource.loadProdutos(
      this.input.nativeElement.value,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  navegarParaCriarProduto(): void {
    this.router.navigate([`produtos/create`])
  }

  findAll(): void {
    this.service.findAll().subscribe((resposta) => {
      this.produtos = resposta;
    })
  }


}


