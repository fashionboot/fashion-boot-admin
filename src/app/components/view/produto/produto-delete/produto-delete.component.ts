import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ProdutoDataSource} from "../../../../services/data-base-produto";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {ProdutoService} from "../../../../services/produto.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MarcaService} from "../../../../services/marca.service";
import {CategoriaService} from "../../../../services/categoria.service";
import {Brand, Category, Gallery, Options, Product} from "../../../../interfaces/interfaces";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-produto-delete',
  templateUrl: './produto-delete.component.html',
  styleUrls: ['./produto-delete.component.css']
})
export class ProdutoDeleteComponent implements OnInit {

  buttonDisable: boolean = false;
  ccheckP: boolean = false;
  ccheckM: boolean = false;
  ccheckG: boolean = false;
  ccheckGG: boolean = false;

  // @ts-ignore
  @ViewChild('checkP') tamanhoP;
  // @ts-ignore
  @ViewChild('checkM') tamanhoM;
  // @ts-ignore
  @ViewChild('checkG') tamanhoG;
  // @ts-ignore
  @ViewChild('checkGG') tamanhoGG;


  // @ts-ignore
  dataSource: ProdutoDataSource;
  fotosColumns: string[] = ["id", "link"];

  constructor(private service: ProdutoService, private route: ActivatedRoute, private router: Router, private marcaService: MarcaService,
              private cateService: CategoriaService,) {

  }

  ngOnInit(): void {
    this.idProduto = this.route.snapshot.paramMap.get("id")!;
    this.findById()
    this.marcaService.findAllBrands().subscribe((resposta) => {
      this.brandd = resposta;
    })
    this.cateService.findAllCategorias().subscribe((resposta) => {
      this.cate = resposta;
    })
    this.service.findGalleryImagensByProdutoId(this.idProduto).subscribe(resposta => {
      this.gallery = resposta;

    })

  }

  idProduto: String = "";

  findById(): void {
    this.service.findById(this.idProduto!).subscribe((resposta) => {
      resposta.options?.forEach(value => {
        if (value.name == 'P') {
          this.ccheckP = true;
        }
        if (value.name == 'M') {
          this.ccheckM = true;
        }
        if (value.name == 'G') {
          this.ccheckG = true;
        }
        if (value.name == 'GG') {
          this.ccheckGG = true;
        }
      })
      this.product = resposta;


    });
  }


  brandd: Brand[] = [];
  cate: Category[] = [];
  category: Category = {

    name: "",
    redirect: ""

  }
  brand: Brand = {

    name: "",
  }
  gallery: Gallery[] = []
  options: Options[] = [];

  product: Product = {
    name: "",
    description: "",
    detail: "",
    category: this.category,
    brand: this.brand,
    img: "",
    gallery: this.gallery,
    onSale: true,
    salePrice: "",
    options: this.options,
    inStock: true,
    gender: "",
  }

  delete(): void {
    this.buttonDisable = true;
    this.service.deleteProduto(this.idProduto).subscribe((resposta) => {
      this.router.navigate(['produtos'])
      this.service.mensagem('produto deletado com sucesso!')
      this.buttonDisable = false;
    }, err => {
      this.service.mensagem(err.error.error)
      this.buttonDisable = false;
    })
  }


  cancel() {
    this.router.navigate(['produtos'])
  }

  name = new FormControl("")
  description = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(100)])
  detail = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(500)])
  categoria = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(30)])
  marca = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(30)])
  img = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(300)])
  onSale = new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(5)])
  salePrice = new FormControl("", [Validators.required, Validators.maxLength(30)])
  inStock = new FormControl("", [Validators.required, Validators.minLength(3)] )
  gender = new FormControl("", [Validators.required])

}
