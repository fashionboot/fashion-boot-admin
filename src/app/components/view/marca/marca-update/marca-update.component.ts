import { Component, OnInit } from '@angular/core';
import {MarcaService} from "../../../../services/marca.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl, Validators} from "@angular/forms";
import {Brand} from "../../../../interfaces/interfaces";

@Component({
  selector: 'app-marca-update',
  templateUrl: './marca-update.component.html',
  styleUrls: ['./marca-update.component.css']
})
export class MarcaUpdateComponent implements OnInit {

  constructor(private service: MarcaService, private router: Router,   private route: ActivatedRoute,) {}
  buttonDisable: boolean = false;

  ngOnInit(): void {
    this.marca.id = this.route.snapshot.paramMap.get("id")!;
    this.service.findById(this.marca.id).subscribe(res => {
      this.marca = res;
    })
  }

  editar(): void {
    this.buttonDisable = true;
    this.service.update(this.marca).subscribe((resposta) => {
      this.router.navigate(['marcas'])
      this.service.mensagem('Marca editada com sucesso!');
      this.buttonDisable = false;
    }, err => {
      this.buttonDisable = false;
      for(let i = 0; i < err.error.errors.length; i++) {
        this.service.mensagem(err.error.errors[i].message)
      }
    })
  }


  cancel() {
    this.router.navigate(['marcas'])
  }


  getMessage() {
    if (this.nome.invalid) {
      return "O campo deve conter pelo menos 1 letra";
    }
    return false;
  }

  nome = new FormControl("", [Validators.minLength(1)]);

  marca: Brand = {
    name: "",
  };
}
