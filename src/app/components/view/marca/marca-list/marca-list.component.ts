import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Brand} from "../../../../interfaces/interfaces";
import {Router} from "@angular/router";
import {MarcaService} from "../../../../services/marca.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {fromEvent, merge} from "rxjs";
import {debounceTime, distinctUntilChanged, tap} from "rxjs/operators";
import {ProdutoDataSource} from "../../../../services/data-base-produto";
import {DataBaseBrand} from "../../../../services/data-base-brand";

@Component({
  selector: 'app-marca-list',
  templateUrl: './marca-list.component.html',
  styleUrls: ['./marca-list.component.css']
})
export class MarcaListComponent implements OnInit, AfterViewInit {

  marcas: Brand[] = [];
  displayedColumns: string[] = ["id", "nome", "acoes"];
  // @ts-ignore
  dataSource: DataBaseBrand;
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild('input') input: ElementRef;

  constructor(private service: MarcaService, private router: Router) {}

  ngOnInit(): void {
    this.dataSource = new DataBaseBrand(this.service);
    this.dataSource.loadProdutos('', 'asc', 0, 5);
    this.findAll();
  }

  findAll() {
    this.service.findAllBrands().subscribe((resposta) => {
      this.marcas = resposta;
    });
  }


  createMarca() {
    this.router.navigate(["marcas/create"])
  }

  ngAfterViewInit(): void {
    // server-side search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),

        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadProdutosPage();
        })
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadProdutosPage())
      )
      .subscribe();
  }


  loadProdutosPage() {

    this.dataSource.loadProdutos(
      this.input.nativeElement.value,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

}

