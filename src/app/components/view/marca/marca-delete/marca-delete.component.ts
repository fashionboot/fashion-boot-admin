import { Component, OnInit } from '@angular/core';
import {MarcaService} from "../../../../services/marca.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl, Validators} from "@angular/forms";
import {Brand} from "../../../../interfaces/interfaces";

@Component({
  selector: 'app-marca-delete',
  templateUrl: './marca-delete.component.html',
  styleUrls: ['./marca-delete.component.css']
})
export class MarcaDeleteComponent implements OnInit {

  id: string = '';
  constructor(private service: MarcaService, private router: Router,   private route: ActivatedRoute,) {}


  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("id")!;
    this.service.findById(this.id).subscribe(res => {
      this.marca = res;
    })
  }

  deletar(): void {
    this.service.delete(this.id).subscribe((resposta) => {
      this.router.navigate(['marcas'])
      this.service.mensagem('Marca Deletada com sucesso!');
    }, err => {
      this.service.mensagem(err.error.error)
    })
  }


  cancel() {
    this.router.navigate(['marcas'])
  }


  nome = new FormControl("", [Validators.minLength(1)]);
  // @ts-ignore
  marca: Brand = null;
}
