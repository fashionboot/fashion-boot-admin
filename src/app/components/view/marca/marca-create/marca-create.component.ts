import {Component, OnInit} from '@angular/core';
import {MarcaService} from "../../../../services/marca.service";
import {Router} from "@angular/router";
import {Brand} from "../../../../interfaces/interfaces";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-marca-create',
  templateUrl: './marca-create.component.html',
  styleUrls: ['./marca-create.component.css']
})
export class MarcaCreateComponent implements OnInit {

  constructor(private service: MarcaService, private router: Router) {
  }

  buttonDisable: boolean = false;

  ngOnInit(): void {
  }

  create(): void {
    this.buttonDisable = true;
    this.service.create(this.marca).subscribe((resposta) => {
      this.router.navigate(['marcas']);
      this.service.mensagem('Marca criada com sucesso!');
      this.buttonDisable = false;

    }, err => {
      this.buttonDisable = false;
      this.service.mensagem(err.error.error);
    })
  }


  cancel() {
    this.router.navigate(['marcas'])
  }


  getMessage() {
    if (this.nome.invalid) {
      return "O campo deve conter pelo menos 1 letra";
    }

    return false;
  }

  nome = new FormControl("", [Validators.minLength(1)]);

  marca: Brand = {
    name: "",
  };
}
