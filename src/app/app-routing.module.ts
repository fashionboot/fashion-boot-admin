import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './components/view/dashboard/dashboard.component';
import {ItemOrderedListComponent} from "./components/view/item_ordered/item-ordered-list/item-ordered-list.component";
import {MarcaCreateComponent} from "./components/view/marca/marca-create/marca-create.component";
import {MarcaDeleteComponent} from "./components/view/marca/marca-delete/marca-delete.component";
import {MarcaListComponent} from "./components/view/marca/marca-list/marca-list.component";
import {MarcaUpdateComponent} from "./components/view/marca/marca-update/marca-update.component";
import {PedidoEditComponent} from "./components/view/pedidos/pedido-edit/pedido-edit.component";
import {PedidoListComponent} from "./components/view/pedidos/pedido-list/pedido-list.component";
import {ProdutoCreateComponent} from "./components/view/produto/produto-create/produto-create.component";
import {ProdutoDeleteComponent} from "./components/view/produto/produto-delete/produto-delete.component";
import {ProdutoListComponent} from "./components/view/produto/produto-list/produto-list.component";
import {ProdutoUpdateComponent} from "./components/view/produto/produto-update/produto-update.component";
import {AuthGuard} from "./guard/app.guard";


const routes: Routes = [

  {
    path: 'produtos',
    component: ProdutoListComponent,
    canActivate: [AuthGuard],


  },
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'produtos/create',
    component: ProdutoCreateComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'produtos/update/:id',
    component: ProdutoUpdateComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'produtos/delete/:id',
    component: ProdutoDeleteComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'marcas',
    component: MarcaListComponent,
    canActivate: [AuthGuard],

  },
  {
    path: 'marcas/create',
    component: MarcaCreateComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'marcas/update/:id',
    component: MarcaUpdateComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'marcas/delete/:id',
    component: MarcaDeleteComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'pedidos',
    component: PedidoListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'pedidos/editar/:id',
    component: PedidoEditComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'pedidos/itemOrdered/:id',
    component: ItemOrderedListComponent,
    canActivate: [AuthGuard],
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
