import { HttpClientModule } from '@angular/common/http';
import {APP_INITIALIZER, DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from "@angular/material/select";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatSortModule } from "@angular/material/sort";
import { MatTableModule } from "@angular/material/table";
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataTablesModule } from 'angular-datatables';
import { ChartModule } from 'angular-highcharts';
import { MatFileUploadModule } from "angular-material-fileupload";
import {KeycloakAngularModule, KeycloakService} from "keycloak-angular";
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/view/dashboard/dashboard.component';
import { GraphicColumnComponent } from './components/view/dashboard/graphic-column/graphic-column.component';
import { GraphicPizzaComponent } from './components/view/dashboard/graphic-pizza/graphic-pizza.component';
import { OrderTableComponent } from './components/view/dashboard/order-table/order-table.component';
import { ItemOrderedListComponent } from './components/view/item_ordered/item-ordered-list/item-ordered-list.component';
import { MarcaCreateComponent } from './components/view/marca/marca-create/marca-create.component';
import { MarcaDeleteComponent } from './components/view/marca/marca-delete/marca-delete.component';
import { MarcaListComponent } from './components/view/marca/marca-list/marca-list.component';
import { MarcaUpdateComponent } from './components/view/marca/marca-update/marca-update.component';
import { PedidoEditComponent } from './components/view/pedidos/pedido-edit/pedido-edit.component';
import { PedidoListComponent } from './components/view/pedidos/pedido-list/pedido-list.component';
import { ProdutoCreateComponent } from './components/view/produto/produto-create/produto-create.component';
import { ProdutoDeleteComponent } from './components/view/produto/produto-delete/produto-delete.component';
import { ProdutoListComponent } from './components/view/produto/produto-list/produto-list.component';
import { ProdutoUpdateComponent } from './components/view/produto/produto-update/produto-update.component';
import { CurrencyFormat } from './pipe/currency-format';
import {initializer} from "./init/app-init";
import {registerLocaleData} from "@angular/common";
import localePt from '@angular/common/locales/pt';
registerLocaleData(localePt);



@NgModule({
  declarations: [
    AppComponent,
    ProdutoListComponent,
    ProdutoCreateComponent,
    ProdutoUpdateComponent,
    ProdutoDeleteComponent,
    MarcaListComponent,
    MarcaCreateComponent,
    MarcaUpdateComponent,
    MarcaDeleteComponent,
    PedidoListComponent,
    ItemOrderedListComponent,
    PedidoEditComponent,
    DashboardComponent,
    GraphicPizzaComponent,
    GraphicColumnComponent,
    OrderTableComponent,
    CurrencyFormat
  ],
  imports: [
    MatDialogModule,
    MatRadioModule,
    MatSortModule,
    MatProgressSpinnerModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatTableModule,
    HttpClientModule,
    MatButtonModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatSelectModule,
    MatFileUploadModule,
    MatCheckboxModule,
    AppRoutingModule,
    KeycloakAngularModule,
    ChartModule,
    DataTablesModule,
    CurrencyMaskModule
  ],
  exports: [
    CurrencyFormat
  ],
  providers: [

    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      deps: [KeycloakService],
      multi: true,
    },
    {
      provide:  DEFAULT_CURRENCY_CODE,
      useValue: 'pt-BR'
    },
    { provide: LOCALE_ID, useValue: "pt-BR" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
