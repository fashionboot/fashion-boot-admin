export interface Address{
  id?:string;
  road:string;
  number:string;
  complement:string;
  district:string;
  cep:string;
  city:City;
}
export interface City{
  id?:string;
  name:string;
  state:State;
}
export interface State{
  id?:string;
  name:string;
  initials:string;
}
export interface Product{
  id?: string;
  name: string;
  description: string;
  detail: string;
  category: Category;
  brand:Brand;
  img: string;
  gallery?: Gallery[];
  onSale: boolean;
  salePrice?: string;
  options?: Options[];
  inStock: boolean;
  gender:string;

}
export interface Gallery{
  id?: string;
  name: string;
}

export interface Options {

  id?: string;
  value: string
  name: string;
}
export interface Category {
  id?: string;
  name: string;
  redirect: string;
  count?: number;
  products?: Product[];
}

export interface Brand{
  id?: string;
  name: string;
  products?: Product[];
}

export interface FormPayment{
  id?:string;
  number?:string;
  flag:string;
  cvv?:string;
  validate:string;
}
export interface Usuario{
  id?:string;
  name:string;
  email:string;
  password:string
  type:string;
  phone:string;

  form_payment:FormPayment;
  address:Address;
  ordemList:Ordem;
}
export interface Ordem{
  id:string;
  date_purchase:string;
  status:string;
  usuario:Usuario;
  totalPrice?:string;
}

export interface ItemOrdered{
  id?:string;
  quantity?:string; //quantidade
  ordem:Ordem;
  product:Product;
}

export interface SaleByMonthDTO {
  monthNumber:string;
  monthDescription:string;
  totalValue:number;
}

export interface OrderByStatusDTO {
  status:string;
  quantity:number;
  color:string;
}

export interface OrderByDayDTO {
  orderNumber:bigint;
  userName:string;
  phone:string;
  totalValue:number;
  status:string;
}
