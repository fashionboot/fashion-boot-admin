import { Component } from '@angular/core';
import {MatIconRegistry} from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";
import {AuthService} from "./guard/auth.service";
import {Usuario} from "./interfaces/interfaces";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{

   loggedUser: any;

  constructor(
    private authService: AuthService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ){

     this.loggedUser = this.authService.getLoggedUser();


    this.matIconRegistry.addSvgIcon(
      "Roupa",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/clothing.svg"),

    )
    this.matIconRegistry.addSvgIcon(
      "brand",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/brand.svg"),

    )
    this.matIconRegistry.addSvgIcon(
      "pedido",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/checklist.svg"),

    )
    this.matIconRegistry.addSvgIcon(
      "pessoa",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/group.svg"),

    )
    this.matIconRegistry.addSvgIcon(
      "home",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/house.svg"),

    )
    this.matIconRegistry.addSvgIcon(
      "fashion",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/dress.svg"),

    )



  }

  logout() {
    this.authService.logout();
  }
}
