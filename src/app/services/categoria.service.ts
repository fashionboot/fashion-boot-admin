import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Category} from "../interfaces/interfaces";

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {


  baseUrl: String = environment.baseUrl;

  constructor(private http: HttpClient) {

  }
  //Pegando todas as Categorias
  findAllCategorias(): Observable<Category[]> {
    const url = `${this.baseUrl}/categorias`;
    return this.http.get<Category[]>(url);
  }

}
