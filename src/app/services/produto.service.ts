import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest, HttpResponse} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Observable} from "rxjs";
import {Gallery, Product} from "../interfaces/interfaces";
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  baseUrl: String = environment.baseUrl

  constructor(private http: HttpClient, private _snack: MatSnackBar) {
  }

  // @ts-ignore
  findAll(): Observable<Product[]> {
    const url = `${this.baseUrl}/products`
    return this.http.get<Product[]>(url)
  }

  findPage(
    nome = '', sortOrder = 'asc',
    pageNumber = 0, pageSize = 10): Observable<Product[]> {
    const url = `${this.baseUrl}/products/page?`
    return this.http.get((url), {
      params: new HttpParams()
        .set('name', nome)
        .set('sortOrder', sortOrder)
        .set('page', pageNumber.toString())
        .set('size', pageSize.toString())
    }).pipe(
      // @ts-ignore
      map(res => res["content"])
    );
  }

  findById(id: any): Observable<Product> {
    const url = `${this.baseUrl}/products/${id}`
    return this.http.get<Product>(url)
  }

  findGalleryImagensByProdutoId(id: any): Observable<Gallery[]>{
    const url = `${this.baseUrl}/galleryImages/produto/${id}`
    return this.http.get<Gallery[]>(url)
  }

  // @ts-ignore
  public upload(formData) {
    const url = `${this.baseUrl}/files/update`
    let options: HttpParams = new HttpParams();
    let headers = new HttpHeaders();
    return this.http.post<any>(url, formData, {
      headers : headers,
      params: options,
      observe: 'response'
    });
  }
  // @ts-ignore
  public uploadReplace(formData) {
    const url = `${this.baseUrl}/files/update`
    let options: HttpParams = new HttpParams();
    let headers = new HttpHeaders();
    return this.http.put<any>(url, formData, {
      headers : headers,
      params: options,
      observe: 'response'
    });
  }

  deleteOptions(id: any): Observable<void> {
    const url = `${this.baseUrl}/options/product/${id}`
    return this.http.delete<void>(url)
  }

  deleteGalleryById(id: any): Observable<void>  {
    const url = `${this.baseUrl}/galleryImages/${id}`
    return this.http.delete<void>(url)
  }
  mensagem(str: String): void {
    this._snack.open(`${str}`, 'OK', {
      horizontalPosition: 'end',
      verticalPosition: 'top',
      duration: 3000
    })
  }


  deleteProduto(id: any) {
    const url = `${this.baseUrl}/products/${id}`
    return this.http.delete<void>(url)
  }
}
