import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Ordem} from "../interfaces/interfaces";
import {BehaviorSubject, Observable, of} from "rxjs";
import {ProdutoService} from "./produto.service";
import {catchError, finalize} from "rxjs/operators";
import {MarcaService} from "./marca.service";
import {OrdemService} from "./ordem.service";

export class DataBaseOrdem implements DataSource<Ordem> {

  private ordemSubject = new BehaviorSubject<Ordem[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private ordemService: OrdemService) {}

  connect(collectionViewer: CollectionViewer): Observable<Ordem[]> {
    return this.ordemSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.ordemSubject.complete();
    this.loadingSubject.complete();
  }


  loadPedidos(filter = '',
               sortDirection = 'asc', pageIndex = 0, pageSize = 3) {

    this.loadingSubject.next(true);

    this.ordemService.findPage(filter, sortDirection,
      pageIndex, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(ordem => this.ordemSubject.next(ordem));
  }

  loadProduto(id : any) {

    this.loadingSubject.next(true);


    this.ordemService.findById(id).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      // @ts-ignore
      .subscribe(lessons => this.ordemSubject.next(lessons));
  }
}
