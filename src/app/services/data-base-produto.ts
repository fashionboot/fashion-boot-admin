import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {BehaviorSubject, Observable, of} from "rxjs";
import {catchError, finalize} from "rxjs/operators";
import {Product} from "../interfaces/interfaces";
import {ProdutoService} from "./produto.service";

export class ProdutoDataSource implements DataSource<Product> {

  private produtoSubject = new BehaviorSubject<Product[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private produtoService: ProdutoService) {}

  connect(collectionViewer: CollectionViewer): Observable<Product[]> {
    return this.produtoSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.produtoSubject.complete();
    this.loadingSubject.complete();
  }


  loadProdutos(filter = '',
               sortDirection = 'asc', pageIndex = 0, pageSize = 3) {

    this.loadingSubject.next(true);

    this.produtoService.findPage(filter, sortDirection,
      pageIndex, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(lessons => this.produtoSubject.next(lessons));
  }

  loadProduto(id : any) {

    this.loadingSubject.next(true);


    this.produtoService.findById(id).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      // @ts-ignore
      .subscribe(lessons => this.produtoSubject.next(lessons));
  }



  // @ts-ignore
  saveProduto(formdata){
    this.loadingSubject.next(true);

    return this.produtoService.upload(formdata).pipe(
     catchError(() => of([])),
     finalize(() => this.loadingSubject.next(false))
   )   // @ts-ignore
     .subscribe(lessons => this.produtoSubject.next(lessons));
  }

  mensagem(str: String){
    return this.produtoService.mensagem(str)
  }
}
