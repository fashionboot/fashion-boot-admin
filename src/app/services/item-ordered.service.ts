import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpParams} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Observable} from "rxjs";
import {Brand, ItemOrdered} from "../interfaces/interfaces";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ItemOrderedService {

  baseUrl: String = environment.baseUrl

  constructor(private http: HttpClient, private _snack: MatSnackBar) { }

  findAllItemOrderedByOrdemId(id: any): Observable<ItemOrdered[]> {
    const url = `${this.baseUrl}/item_ordered/orders/${id}`;
    return this.http.get<ItemOrdered[]>(url);

  }

  findPage(
    nome = '', sortOrder = 'asc',
    pageNumber = 0, pageSize = 5): Observable<ItemOrdered[]> {
    const url = `${this.baseUrl}/item_ordered/page?`
    return this.http.get((url), {
      params: new HttpParams()
        .set('name', nome)
        .set('sortOrder', sortOrder)
        .set('page', pageNumber.toString())
        .set('size', pageSize.toString())
    }).pipe(
      // @ts-ignore
      map(res => res["content"])
    );
  }
}
