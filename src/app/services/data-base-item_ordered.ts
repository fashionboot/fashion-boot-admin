import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Brand, ItemOrdered} from "../interfaces/interfaces";
import {BehaviorSubject, Observable, of} from "rxjs";
import {ProdutoService} from "./produto.service";
import {catchError, finalize} from "rxjs/operators";
import {MarcaService} from "./marca.service";
import {ItemOrderedService} from "./item-ordered.service";

export class DataBaseBrand implements DataSource<ItemOrdered> {

  private ItemOrderedSubject = new BehaviorSubject<ItemOrdered[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private itemOrderedService: ItemOrderedService) {
  }

  connect(collectionViewer: CollectionViewer): Observable<ItemOrdered[]> {
    return this.ItemOrderedSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.ItemOrderedSubject.complete();
    this.loadingSubject.complete();
  }


  loadProdutos(filter = '',
               sortDirection = 'asc', pageIndex = 0, pageSize = 5) {

    this.loadingSubject.next(true);

    this.itemOrderedService.findPage(filter, sortDirection,
      pageIndex, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(lessons => this.ItemOrderedSubject.next(lessons));
  }

  loadProduto(id: any) {

    this.loadingSubject.next(true);


  }
}
