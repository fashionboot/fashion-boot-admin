import { Ordem, OrderByDayDTO, OrderByStatusDTO, SaleByMonthDTO } from './../interfaces/interfaces';
import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpParams} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class OrdemService {

  baseUrl: String = environment.baseUrl

  constructor(private http: HttpClient, private _snack: MatSnackBar) { }

  findAll(): Observable<Ordem[]> {
    const url = `${this.baseUrl}/orders`;
    return this.http.get<Ordem[]>(url);

  }

  findPage(
    nome = '', sortOrder = 'asc',
    pageNumber = 0, pageSize = 5): Observable<Ordem[]> {
    const url = `${this.baseUrl}/orders/page?`
    return this.http.get((url), {
      params: new HttpParams()
        .set('name', nome)
        .set('sortOrder', sortOrder)
        .set('page', pageNumber.toString())
        .set('size', pageSize.toString())
    }).pipe(
      // @ts-ignore
      map(res => res["content"])
    );
  }

  create(ordem: Ordem): Observable<Ordem> {
    const url = `${this.baseUrl}/orders`
    return this.http.post<Ordem>(url, ordem);
  }
  update(ordem: Ordem): Observable<Ordem> {
    const url = `${this.baseUrl}/orders`
    return this.http.put<Ordem>(url, ordem);
  }
  delete(id: any): Observable<void>{
    const url = `${this.baseUrl}/orders/${id}`
    return this.http.delete<void>(url)
  }
  findById(id: any): Observable<Ordem> {
    const url = `${this.baseUrl}/orders/${id}`
    return this.http.get<Ordem>(url)
  }

  findSaleByMonth(): Observable<SaleByMonthDTO[]> {
    const url = `${this.baseUrl}/orders/sale-by-month`
    return this.http.get<SaleByMonthDTO[]>(url)
  }

  findOrderByStatus(): Observable<OrderByStatusDTO[]> {
    const url = `${this.baseUrl}/orders/order-by-status`
    return this.http.get<OrderByStatusDTO[]>(url)
  }

  findOrderByDay(): Observable<OrderByDayDTO[]> {
    const url = `${this.baseUrl}/orders/order-by-day`
    return this.http.get<OrderByDayDTO[]>(url)
  }

  mensagem(str: String): void {
    this._snack.open(`${str}`, 'OK', {
      horizontalPosition: 'end',
      verticalPosition: 'top',
      duration: 3000
    })
  }

}
