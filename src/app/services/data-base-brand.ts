import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Brand} from "../interfaces/interfaces";
import {BehaviorSubject, Observable, of} from "rxjs";
import {ProdutoService} from "./produto.service";
import {catchError, finalize} from "rxjs/operators";
import {MarcaService} from "./marca.service";

export class DataBaseBrand implements DataSource<Brand> {

  private brandSubject = new BehaviorSubject<Brand[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private produtoService: MarcaService) {}

  connect(collectionViewer: CollectionViewer): Observable<Brand[]> {
    return this.brandSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.brandSubject.complete();
    this.loadingSubject.complete();
  }


  loadProdutos(filter = '',
               sortDirection = 'asc', pageIndex = 0, pageSize = 3) {

    this.loadingSubject.next(true);

    this.produtoService.findPage(filter, sortDirection,
      pageIndex, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(lessons => this.brandSubject.next(lessons));
  }

  loadProduto(id : any) {

    this.loadingSubject.next(true);


    this.produtoService.findById(id).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      // @ts-ignore
      .subscribe(lessons => this.brandSubject.next(lessons));
  }
}
