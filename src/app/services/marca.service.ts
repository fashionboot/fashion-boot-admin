import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpParams} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Observable} from "rxjs";
import {Brand, Product} from "../interfaces/interfaces";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MarcaService {

  baseUrl: String = environment.baseUrl

  constructor(private http: HttpClient, private _snack: MatSnackBar) { }

  //metodo para trazer todas as MARCAS
  findAllBrands(): Observable<Brand[]> {
    const url = `${this.baseUrl}/brands`;
    return this.http.get<Brand[]>(url);

  }

  findPage(
    nome = '', sortOrder = 'asc',
    pageNumber = 0, pageSize = 10): Observable<Brand[]> {
    const url = `${this.baseUrl}/brands/page?`
    return this.http.get((url), {
      params: new HttpParams()
        .set('name', nome)
        .set('sortOrder', sortOrder)
        .set('page', pageNumber.toString())
        .set('size', pageSize.toString())
    }).pipe(
      // @ts-ignore
      map(res => res["content"])
    );
  }

  create(marca: Brand): Observable<Brand> {
    const url = `${this.baseUrl}/brands`
    return this.http.post<Brand>(url, marca);
  }
  update(marca: Brand): Observable<Brand> {
    const url = `${this.baseUrl}/brands`
    return this.http.put<Brand>(url, marca);
  }
  delete(id: any): Observable<void>{
    const url = `${this.baseUrl}/brands/${id}`
    return this.http.delete<void>(url)
  }
  findById(id: any): Observable<Brand> {
    const url = `${this.baseUrl}/brands/${id}`
    return this.http.get<Brand>(url)
  }

  mensagem(str: String): void {
    this._snack.open(`${str}`, 'OK', {
      horizontalPosition: 'end',
      verticalPosition: 'top',
      duration: 3000
    })
  }

}
